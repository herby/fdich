# fdich

Fairly distinguishable identifier chars.

A string of chars that are letters (usable in identifiers) and are fairly distinguishable from each other, visually.